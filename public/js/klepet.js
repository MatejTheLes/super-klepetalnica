// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  if(kanal=="dregljaj"){
    var posiljatelj = $('#kanal').text().split(" ")[0];
    var sporocilo = {
      kanal: kanal,
      vzdevek: besedilo,
      posiljatelj: posiljatelj
    };
    socket.emit('dregljaj',sporocilo);
  }else{
    var sporocilo = {
     kanal: kanal,
     besedilo: besedilo
    };
    this.socket.emit('sporocilo', sporocilo);
  }  
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      var tekst = parametri[3];
      var zacetek = tekst.substring(0, 4);
      var konec = tekst.substr(tekst.length - 3);
      if(zacetek == "http" && (konec == "jpg" || konec == "gif" || konec == "png")){
         this.socket.emit('sporocilo', "Do sem mi je uspelo narediti...(zaznava)");
      }
      else{
        if (parametri) {
          this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3]});
          sporocilo = '(zasebno za ' + parametri[1] + '): ' + parametri[3] + konec;
        } else {
          sporocilo = 'Neznan ukaz';
        }
      }
      break;
      case 'dregljaj':
      besede.shift();
      var vzdevek = besede.join(' ');
      var uporabniki=$('#seznam-uporabnikov div');
      var napaka=true;
      for (var i=0;i<uporabniki.length;i++){
        if(uporabniki[i].innerHTML == vzdevek){
          napaka=false;
          break;
        }
      }
      if (!vzdevek){
        sporocilo = "Neznan ukaz"
      }else if(!napaka){
        sporocilo = "Dregljaj za "+vzdevek; 
        this.posljiSporocilo('dregljaj',vzdevek);
      }else{
        //sporocilo = "Uporabnik ne obstaja!"  
        sporocilo = "Neznan ukaz";
      }
      break;
    case 'barva':
      besede.shift();
      var besedilo = besede.join(' ');
      document.querySelector("#sporocila").style.backgroundColor = besedilo;
      break;
    case 'preimenuj':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      var user = parametri[3] + " (" + parametri[1] + ")";
      this.socket.emit('vzdevekSpremembaZahteva', user);
      break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};